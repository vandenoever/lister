A simple program to list all files in a directory.

It is a demonstration of the speed you can achieve with asynchronous code.

Directory listing is rarely done asynchronously. GNU find uses only one thread. This means that the program is waiting a lot for data to come back from the disk. Even with an SSD, this takes time.

This program has a different approach. The directories are listed in thread pool. For a cold cache, it can be 3 times faster than find. For a warm cache it is slightly faster.

This code is a demonstration program. Ideally a version of the idea will be adaped in WalkDir.

To compile the program and list all files in your home directory, run this:

```bash
git clone https://vandenoever@gitlab.com/vandenoever/lister.git
cd lister
cargo build --release
target/release/lister ~
```

To clear the file cache on Linux, run this:

```bash
echo 3 > /proc/sys/vm/drop_caches
```

# Heisenbug

Unfortunately, this code has a bug. It may simply hang. To reproduce this bug, run this:

```bash
while true; do ./target/release/lister ~ | wc -l; done
```

The main iterator thread posts jobs to waiter threads. The waiter threads report the read directory entries to the waiter thread. After each directory they send a message that the directory was read completely. The iterator counts how many jobs are sent out and how many are reported as finished. When the job count is zero, the iterator is done.

Sometimes some jobs are not reported back to the iterator bug. This means that the iterator is never done and the program halts.

It is not clear why some jobs are lost.
