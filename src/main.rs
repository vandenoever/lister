extern crate spmc;
use std::env;
use std::fs::DirEntry;
use std::io::{self, Write};
use std::path::PathBuf;
use std::sync::mpsc;
use std::{thread, time};

macro_rules! wout { ($($tt:tt)*) => { {writeln!($($tt)*)}.unwrap() } }

struct StrErr {
    string: String
}

impl<E: std::error::Error> std::convert::From<E> for StrErr {
    fn from(e: E) -> StrErr {
        StrErr { string: e.description().into() }
    }
}

enum WaiterRequest {
    Clone,
    Job(PathBuf)
}

enum WaiterResponse {
    DirEntry(DirEntry),
    Err(StrErr),
    DirDone,
}

fn waiter_wait(irx: &spmc::Receiver<WaiterRequest>, otx: &mpsc::Sender<WaiterResponse>) -> Result<(), StrErr> {
    match try!(irx.recv()) {
        WaiterRequest::Clone => {
            waiter(irx.clone(), otx.clone());
        }
        WaiterRequest::Job(path) => {
            if let Ok(dir) = path.read_dir() {
                for f in dir {
                    match f {
                        Ok(f) => try!(otx.send(WaiterResponse::DirEntry(f))),
                        Err(_) => {},
                    }
                }
            }
            try!(otx.send(WaiterResponse::DirDone));
        }
    }
    Ok(())
}

fn waiter(irx: spmc::Receiver<WaiterRequest>, otx: mpsc::Sender<WaiterResponse>) -> thread::JoinHandle<()> {
    thread::spawn(move || {
        loop {
            if let Err(e) = waiter_wait(&irx, &otx) {
                if let Err(_) = otx.send(WaiterResponse::Err(e)) {
                    break;
                }
            }
        }
    })
}

struct DirLister {
    itx: spmc::Sender<WaiterRequest>,
    orx: mpsc::Receiver<WaiterResponse>,
    jobs: usize,
    max_waiters: usize,
    nwaiters: usize,
    wait_time: std::time::Duration
}

impl DirLister {
    fn new(path: PathBuf) -> DirLister {
        let (itx, irx) = spmc::channel();
        let (otx, orx) = mpsc::channel();
        waiter(irx, otx);
        itx.send(WaiterRequest::Job(path)).unwrap();
        DirLister {
            itx: itx,
            orx: orx,
            jobs: 1,
            max_waiters: 100,
            nwaiters: 1,
            wait_time: time::Duration::from_millis(20)
        }
    }
}

impl Iterator for DirLister {
    type Item = DirEntry;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.orx.try_recv() {
                Ok(WaiterResponse::DirEntry(entry)) => {
                    let path = entry.path();
                    if entry.file_type().unwrap().is_dir() {
                        if let Err(e) = self.itx.send(WaiterRequest::Job(path.clone())) {
                            println!("receiver hung up {}", e);
                            self.nwaiters -= 1;
                        } else {
                            self.jobs += 1;
                        }
                    }
                    return Some(entry);
                }
                Ok(WaiterResponse::DirDone) => {
                    self.jobs -= 1;
                    if self.jobs == 0 {
                        break;
                    }
                }
                Ok(WaiterResponse::Err(e)) => {
                    println!("{}", e.string);
                    self.jobs -= 1;
                    self.nwaiters -= 1;
                }
                Err(_) => { // listening on an empty channel
                    if self.nwaiters < self.max_waiters {
                        self.nwaiters += 1;
                        self.itx.send(WaiterRequest::Clone).unwrap();
                    }
                    thread::sleep(self.wait_time);
                }
            }
        }
        None
    }
}

fn list(paths: Vec<String>) {
    let mut out = io::BufWriter::new(io::stdout());
    for p in paths {
        wout!(out, "{}", p);
        out.flush().unwrap();
        let it = DirLister::new(p.into());
        for i in it {
            wout!(out, "{}", i.path().to_string_lossy());
        }
    }
}

fn main() {
    let mut args = env::args();
    args.next();
    let mut paths = Vec::new();
    for path in args {
        paths.push(path);
    }
    list(paths);
}
